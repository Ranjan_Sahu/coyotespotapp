//
//  Utilities.m
//  CoyoteSpot
//
//  Created by Admin on 25/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import "Utilities.h"

@implementation Utilities
+(BOOL) validateEmail:(NSString*) emailString
{
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    if (regExMatches == 0) {
        return NO;
    }
    else
        return YES;
}
+ (BOOL)validatePhone:(NSString *)phoneNumber
{
    NSString *phoneRegex = @"^((\\+)|(00))[0-9]{6,14}$";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    
    return [phoneTest evaluateWithObject:phoneNumber];
}

+(void)showAlertWithTitle:(NSString*)title Message:(NSString*)message CancelButtonTitle:(NSString*)cancelButtonTitle InView:(id)viewController
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                   message:message
                                                            preferredStyle:(UIAlertControllerStyleAlert)];
    
    UIAlertAction *alert_cancel_action = [UIAlertAction actionWithTitle:cancelButtonTitle
                                                                  style:(UIAlertActionStyleCancel)
                                                                handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                }];
    [alert addAction:alert_cancel_action];
    [viewController presentViewController:alert animated:YES completion:nil];
}

+(BOOL)validateOnlyNumeric: (NSString *)fieldText
{
    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    if ([fieldText rangeOfCharacterFromSet:notDigits].location == NSNotFound)
        return TRUE;
    else
        return FALSE;
}

@end
