//
//  Utilities.h
//  CoyoteSpot
//
//  Created by Admin on 25/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utilities : NSObject
+(BOOL) validateEmail:(NSString*) emailString;
+(BOOL)validatePhone:(NSString *)phoneNumber;
+(void)showAlertWithTitle:(NSString*)title Message:(NSString*)message CancelButtonTitle:(NSString*)cancelButtonTitle InView:(id)viewController;
+(BOOL)validateOnlyNumeric: (NSString *)fieldText;
@end
