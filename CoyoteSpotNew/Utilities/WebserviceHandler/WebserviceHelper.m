//
//  WebserviceHelper.m
//  TestChoiceTech
//
//  Created by Admin on 16/10/17.
//  Copyright © 2017 Neosoft. All rights reserved.
//

#import "WebserviceHelper.h"
#import "Reachability.h"

@implementation WebserviceHelper
#pragma mark - Webservice function
-(void)getJsonResponse:(NSString *)urlStr viewcontroller:(UIViewController*)controller success:(void (^)(NSDictionary *response))success failure:(void(^)(NSError* error))failure
{
    [self checkNetwork:controller];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURL *url = [NSURL URLWithString:urlStr];
    NSURLSessionDataTask *dataTask = [session dataTaskWithURL:url
                                            completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                [self removeSpinner:controller];
                                                if (error)
                                                    failure(error);
                                                else {
                                                    NSDictionary *json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                    //NSLog(@"%@",json);
                                                    success(json);
                                                }
                                            }];
    [dataTask resume];    // Executed First
}
-(void)postJsonResponse:(NSString *)urlStr params:(NSMutableDictionary*)parameter viewcontroller:(UIViewController*)controller success:(void (^)(NSDictionary *response))success failure:(void(^)(NSError* error))failure
{
    [self checkNetwork:controller];
    NSError *error;
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSString *hostUrlStr = host_Url;
    NSString *serviceUrlStr = [hostUrlStr stringByAppendingString:urlStr];
    NSURL *url = [NSURL URLWithString:serviceUrlStr];
    
    // Generate JSON String
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameter options:NSJSONWritingPrettyPrinted error:nil];
    if (jsonData){
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",jsonString);
    }
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                   cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setHTTPMethod:@"POST"];
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameter options:0 error:&error];
    [request setHTTPBody:postData];
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        [self removeSpinner:controller];
        if (error)
            failure(error);
        else {
            NSDictionary *json  = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            //NSLog(@"%@",json);
            success(json);
        }
    }];
    [postDataTask resume];
}
#pragma mark - Internet Check
- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}
-(void)checkNetwork:(UIViewController*)controller{
    if (![self connected]) {
        // Not connected
        return;
    } else {
        [self createSpinner:controller];
        // Connected. Do some Internet stuff
    }
}
#pragma mark - Hudder
-(void)createSpinner:(UIViewController*)controller{
    if ([controller.view viewWithTag:1000] == nil){
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicator.alpha = 1.0;
        activityIndicator.tag = 1000;
        [controller.view addSubview:activityIndicator];
        activityIndicator.center = CGPointMake([[UIScreen mainScreen]bounds].size.width/2, [[UIScreen mainScreen]bounds].size.height/2);
        [activityIndicator startAnimating];//to start animating
    }
}
-(void)removeSpinner:(UIViewController*)controller{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIActivityIndicatorView *removeView;
    while((removeView = [controller.view viewWithTag:1000]) != nil) {
            [removeView removeFromSuperview];
        }
    });
}
@end
