//
//  Constants.h
//  CoyoteSpot
//
//  Created by webwerks on 11/8/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#ifndef Constants_h
#define Constants_h
#import "AppDelegate.h"

//hostName Constant
#define host_Url @"http://medicalfraternity.phpdevelopment.co.in/coyotespot/webservices/"

//ServiceName Constants
#define Service_Registration @"User/registration"
#define Service_Login @"User/login"
#define Service_UpdateCurrentLocation @"Device/updateCurrentLocation"
#define Service_UpdateDeviceToken @"Device/updateDeviceToken"
#define Service_SaveEncounter @"Notification/save_encounter"
#define Service_EncounterList @"Notification/list_encounter"

//iOS version Constants
#define IOS_VERSION ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

//system version Constants
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

//App delegate Constant
#define APP_DELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

//key Constants
#define notification_Observer_Const @"COYOTE_ENCOUNTERED_NOTIFICATION"

#endif
