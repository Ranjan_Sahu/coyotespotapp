//
//  AppDelegate.m
//  CoyoteSpot
//
//  Created by Shashi Dayal on 12/30/16.
//  Copyright © 2016 RoboChaps. All rights reserved.
//

#import "AppDelegate.h"
#import <UserNotifications/UserNotifications.h>
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>

#import <Firebase.h>
#import <FirebaseInstanceID/FirebaseInstanceID.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <FirebaseCore/FirebaseCore.h>

#import "SignInViewController.h"
#import "HomeViewController.h"
#import "FormViewController.h"
#import "DetailFormViewController.h"

@interface AppDelegate ()<UIApplicationDelegate,UNUserNotificationCenterDelegate, FIRMessagingDelegate>@end

@implementation AppDelegate
@synthesize window,navController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [GMSServices provideAPIKey:@"AIzaSyCpZbJyBHIy2u_WHxXfTJ2Sdizb0maTW4I"];
    [GMSPlacesClient provideAPIKey:@"AIzaSyCpZbJyBHIy2u_WHxXfTJ2Sdizb0maTW4I"];
    //[self registerForRemoteNotifications];
    [self RegisterForFirebaseRemoteNotifications];
    
    window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    window.backgroundColor = [UIColor whiteColor];
    
    //if ([launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey]) {
    //}
    
    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"USER_ID"] length] == 0)
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *signInVC = [storyBoard instantiateViewControllerWithIdentifier:@"SignInViewController"];
        self.navController = [[UINavigationController alloc] initWithRootViewController:signInVC];
        self.window.rootViewController = self.navController;
    }
    else
    {
        UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *homeVC = [storyBoard instantiateViewControllerWithIdentifier:@"HomeViewController"];
        self.navController = [[UINavigationController alloc] initWithRootViewController:homeVC];
        self.window.rootViewController = self.navController;
    }
    
    [self.window makeKeyAndVisible];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString * deviceTokenString = [[[[deviceToken description] stringByReplacingOccurrencesOfString: @"<" withString: @""] stringByReplacingOccurrencesOfString: @">" withString: @""]   stringByReplacingOccurrencesOfString: @" " withString: @""];
    NSLog(@"Did Register for Remote Notifications with Device Token (%@)", deviceTokenString);
    [[NSUserDefaults standardUserDefaults]setValue:deviceTokenString forKey:@"DEVICE_TOKEN"];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(nonnull NSDictionary *)userInfo fetchCompletionHandler:(nonnull void (^)(UIBackgroundFetchResult))completionHandler
{
    // Post Notifier to MapView
    [[NSNotificationCenter defaultCenter] postNotificationName:notification_Observer_Const object:nil];
}
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"Did Fail to Register for Remote Notifications");
    NSLog(@"%@, %@", error, error.localizedDescription);
}

//Called when a notification is delivered to a foreground app.
-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    //completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
    completionHandler(UNNotificationPresentationOptionSound | UNNotificationPresentationOptionAlert | UNNotificationPresentationOptionBadge);
}

//Called to let your app know which action was selected by the user for a given notification.
//-(void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler{
//    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
//    // Post Notifier to MapView
//    [[NSNotificationCenter defaultCenter] postNotificationName:notification_Observer_Const object:nil];
//    completionHandler();
//}

-(void)RegisterForFirebaseRemoteNotifications
{
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0"))
    {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error){
            if(!error){
                [FIRMessaging messaging].delegate = self;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                });
            }
        }];
    }
    else
    {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert| UIUserNotificationTypeBadge| UIUserNotificationTypeSound) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        dispatch_async(dispatch_get_main_queue(), ^{
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        });
    }
    [FIRApp configure];
}

#pragma mark - FireBase Delegates
-(void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage
{
    NSLog(@"RemoteMessage: %@",remoteMessage);
}
-(void) applicationReceivedRemoteMessage:(FIRMessagingRemoteMessage *)remoteMessage
{
    NSLog(@"RemoteMessage: %@",remoteMessage);
}
-(void)messaging:(FIRMessaging* )messaging didRefreshRegistrationToken:(NSString* )fcmToken
{
    NSLog(@"didRefresh FCM TOKEN: %@",fcmToken);
    [[NSUserDefaults standardUserDefaults]setValue:fcmToken forKey:@"FCM_DEVICE_TOKEN"];
}
-(void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken
{
    NSLog(@"didReceive FCM TOKEN: %@",fcmToken);
    [[NSUserDefaults standardUserDefaults]setValue:fcmToken forKey:@"FCM_DEVICE_TOKEN"];
}
#pragma mark - Other method
-(void)logoutUser
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"USER_ID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *signInVC = [storyBoard instantiateViewControllerWithIdentifier:@"SignInViewController"];
    navController = [[UINavigationController alloc] initWithRootViewController:signInVC];
    window.rootViewController = navController;
    [window makeKeyAndVisible];
}
@end
