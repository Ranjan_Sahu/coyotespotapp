//
//  SignInViewController.h
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utilities.h"

@interface SignInViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *TF_EmailAddress;
@property (weak, nonatomic) IBOutlet UITextField *TF_Password;
@property (weak, nonatomic) IBOutlet UIView *bgView;

- (IBAction)btnSubmitClicked:(id)sender;
- (IBAction)btnSignupClicked:(id)sender;

@end
