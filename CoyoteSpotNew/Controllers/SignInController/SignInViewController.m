//
//  SignInViewController.m
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import "SignInViewController.h"
#import "SignUpViewController.h"
#import "HomeViewController.h"
#import <CoreLocation/CoreLocation.h>

@interface SignInViewController ()<UITextFieldDelegate>
{
    UIToolbar *toolBar;
    UITextField *editingTextField;
}
@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *bgTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBgTap:)];
    [self.bgView addGestureRecognizer:bgTap];
    
    // set delegates
    self.TF_EmailAddress.delegate = self;
    self.TF_Password.delegate = self;
    self.TF_EmailAddress.text = @"";
    self.TF_Password.text = @"";
    
    // Toobar for Done button
    toolBar = [[UIToolbar alloc] init];
    [toolBar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(didSelectToolBarDone)];
    toolBar.items = @[flexBarButton, doneBarButton];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

#pragma mark - TextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    editingTextField = textField;
    textField.inputAccessoryView = toolBar;
    [textField becomeFirstResponder];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    editingTextField = nil;
    [textField resignFirstResponder];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return true;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return true;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 80;
}

#pragma mark - Custom methods
-(void)didSelectToolBarDone
{
    [editingTextField resignFirstResponder];
}

#pragma mark - IBAction methods
- (IBAction)btnSignupClicked:(id)sender
{
    [self.view endEditing:YES];
    SignUpViewController *signup = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    [self.navigationController pushViewController:signup animated:true];
}

-(IBAction)btnSubmitClicked:(UIButton*)sender
{
    [self.view endEditing:true];
    NSMutableDictionary *paramdict = [[NSMutableDictionary alloc]init];
    if (!(self.TF_EmailAddress.text.length>0)) {
        [Utilities showAlertWithTitle:@"" Message:@"Please enter your email address !" CancelButtonTitle:@"Ok" InView:self];
    }
    else if (!(self.TF_Password.text.length>0)) {
        [Utilities showAlertWithTitle:@"" Message:@"Please enter your password !" CancelButtonTitle:@"Ok" InView:self];
    }
    else {
        if (![Utilities validateEmail:self.TF_EmailAddress.text]) {
            [Utilities showAlertWithTitle:@"" Message:@"Please enter valid email address !" CancelButtonTitle:@"Ok" InView:self];
        }
        else if (self.TF_Password.text.length<6) {
            [Utilities showAlertWithTitle:@"" Message:@"Password must be greater than or equal to 6 digits !" CancelButtonTitle:@"Ok" InView:self];
        }
        else {
            paramdict[@"email"] = self.TF_EmailAddress.text;
            paramdict[@"password"] = self.TF_Password.text;
            [self callSignInApi:paramdict];
        }
    }
}

#pragma mark - Webservice Call
-(void)callSignInApi:(NSMutableDictionary*)params
{
    WebserviceHelper *web = [[WebserviceHelper alloc] init];
    [web postJsonResponse:Service_Login params:params viewcontroller:self success:^(NSDictionary *response) {
        //success
        BOOL status = [response[@"status"] boolValue];
        if (!status){
            NSLog(@"SignIn failed !! %@",response[@"message"]);// SignIn failed
            [Utilities showAlertWithTitle:@"" Message:response[@"message"] CancelButtonTitle:@"Ok" InView:self];
        } else {
            NSLog(@"SignIn Success !!");// SignIn Success
            NSString* userid = response[@"user_id"];
            [[NSUserDefaults standardUserDefaults]setValue:userid forKey:@"USER_ID"];
            dispatch_async(dispatch_get_main_queue(), ^{
                HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                APP_DELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:home];
                APP_DELEGATE.window.rootViewController = APP_DELEGATE.navController;
            });
        }
    } failure:^(NSError *error) {
        NSLog(@"%@", error.localizedDescription);//error
    }];
}

-(void)handleBgTap: (UITapGestureRecognizer*)sender
{
    [self.view endEditing:true];
}

@end
