//
//  SignUpViewController.m
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import "SignUpViewController.h"
#import "HomeViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import <GooglePlaces/GooglePlaces.h>
#import <GooglePlacePicker/GooglePlacePicker.h>

@interface SignUpViewController ()<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, GMSPlacePickerViewControllerDelegate>
{
    CLLocationManager *locationManager;
    GMSPlacePickerViewController *placePicker;
    CLLocation *deviceLocation;
    UIImagePickerController *pickerVC;
    UIToolbar *toolBar;
    UITextField *editingTextField;
    NSString *locLatitude, *locLongitude;
}
@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITapGestureRecognizer *imgViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapOnProfilePic:)];
    [self.imgView addGestureRecognizer:imgViewTap];
    self.imgView.userInteractionEnabled = true;
    
    // set delegates
    self.TF_Firstname.delegate = self;
    self.TF_Lastname.delegate = self;
    self.TF_EmailId.delegate = self;
    self.TF_Password.delegate = self;
    self.TF_ReEnterPass.delegate = self;
    self.TF_Mobile.delegate = self;
    self.TF_Address.delegate = self;
    self.TF_GeoLocation.delegate = self;
    
    // Toobar for Done button
    toolBar = [[UIToolbar alloc] init];
    [toolBar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(didSelectToolBarDone)];
    toolBar.items = @[flexBarButton, doneBarButton];
    
    // Confg Location and Map
    locationManager = [[CLLocationManager alloc] init];
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    [locationManager startUpdatingLocation];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    NSString *latitude = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f",locationManager.location.coordinate.longitude];
    deviceLocation = locationManager.location;
    NSLog(@"%@",latitude);
    NSLog(@"%@",longitude);
    [locationManager stopUpdatingLocation];
    
}

#pragma mark - TextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    editingTextField = textField;
    textField.inputAccessoryView = toolBar;
    [textField becomeFirstResponder];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    editingTextField = nil;
    [textField resignFirstResponder];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.TF_Address) {
        [self pickPlaceWithLocation:deviceLocation];
        return false;
    }
    else if (textField == self.TF_GeoLocation) {
        return false;
    }
    else{
        return true;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(range.length + range.location > textField.text.length){
        return NO;
    }
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if ([textField.placeholder isEqual:@"Mobile"]){
        return newLength <= 10;
    }
    return newLength <= 80;
}

#pragma mark - IBAction methods
- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

-(IBAction)btnSubmitClicked:(UIButton*)sender
{
    [self.view endEditing:YES];
    NSMutableDictionary *paramdict = [[NSMutableDictionary alloc]init];
    
    if (!(self.TF_Firstname.text.length>0)) {
        NSLog(@"Please enter first name !");
        [Utilities showAlertWithTitle:@"" Message:@"Please enter first name !" CancelButtonTitle:@"Ok" InView:self];
    }
    else if (!(self.TF_Lastname.text.length>0)) {
        NSLog(@"Please enter last name !");
        [Utilities showAlertWithTitle:@"" Message:@"Please enter last name !" CancelButtonTitle:@"Ok" InView:self];
    }
    else if (!(self.TF_EmailId.text.length>0)) {
        NSLog(@"Please enter your email address !");
        [Utilities showAlertWithTitle:@"" Message:@"Please enter your email address !" CancelButtonTitle:@"Ok" InView:self];
    }
    else if (!(self.TF_Password.text.length>0)) {
        NSLog(@"Please enter password !");
        [Utilities showAlertWithTitle:@"" Message:@"Please enter password !" CancelButtonTitle:@"Ok" InView:self];
    }
    else if (!(self.TF_ReEnterPass.text.length>0)) {
        NSLog(@"Please Re-enter password !");
        [Utilities showAlertWithTitle:@"" Message:@"Please Re-enter password !" CancelButtonTitle:@"Ok" InView:self];
    }
    else if (!(self.TF_Mobile.text.length>0)) {
        NSLog(@"Please enter mobile number !");
        [Utilities showAlertWithTitle:@"" Message:@"Please enter mobile number !" CancelButtonTitle:@"Ok" InView:self];
    }
    else if (!(self.TF_Address.text.length>0)) {
        NSLog(@"Please enter address !");
        [Utilities showAlertWithTitle:@"" Message:@"Please enter address !" CancelButtonTitle:@"Ok" InView:self];
    }
    else {
        if (![Utilities validateEmail:self.TF_EmailId.text]) {
            NSLog(@"Please enter valid email address !");
            [Utilities showAlertWithTitle:@"" Message:@"Please enter valid email address !" CancelButtonTitle:@"Ok" InView:self];
        }
        else if (self.TF_Password.text.length<6) {
            NSLog(@"Password must be greater than or equal to 6 digits !");
            [Utilities showAlertWithTitle:@"" Message:@"Password must be greater than or equal to 6 digits !" CancelButtonTitle:@"Ok" InView:self];
        }
        else if (self.TF_ReEnterPass.text.length<6) {
            NSLog(@"ReEnterPass must be greater than or equal to 6 digits !");
            [Utilities showAlertWithTitle:@"" Message:@"Password must be greater than or equal to 6 digits !" CancelButtonTitle:@"Ok" InView:self];
        }
        else if (self.TF_Password.text != self.TF_ReEnterPass.text) {
            NSLog(@"Password mismatched !");
            [Utilities showAlertWithTitle:@"" Message:@"Password mismatched !" CancelButtonTitle:@"Ok" InView:self];
        }
        else {
            paramdict[@"first_name"] = self.TF_Firstname.text;
            paramdict[@"last_name"] = self.TF_Lastname.text;
            paramdict[@"email"] = self.TF_EmailId.text;
            paramdict[@"password"] = self.TF_Password.text;
            paramdict[@"mobile_no"] = self.TF_Mobile.text;
            paramdict[@"address"] = self.TF_Address.text;
            paramdict[@"latitude"] = locLatitude;
            paramdict[@"longitude"] = locLongitude;
            paramdict[@"profile_image"] = [NSString stringWithFormat:@"%@",[self base64String:self.imgView.image]];
            [self callSignUpApi:paramdict];
        }
    }
}

-(void)callSignUpApi:(NSMutableDictionary*)params
{
    WebserviceHelper *web = [[WebserviceHelper alloc] init];
    [web postJsonResponse:Service_Registration params:params viewcontroller:self success:^(NSDictionary *response) {
        //success
        BOOL status = [response[@"status"] boolValue];
        if (!status){
            NSLog(@"Registeration failed");// registeration failed
            [Utilities showAlertWithTitle:@"" Message:response[@"message"] CancelButtonTitle:@"Ok" InView:self];
        } else {
            NSLog(@"Registeration success");// success registeration done
            NSString* userid = response[@"user_id"];
            [[NSUserDefaults standardUserDefaults]setValue:userid forKey:@"USER_ID"];
            dispatch_async(dispatch_get_main_queue(), ^{
                HomeViewController *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                APP_DELEGATE.navController = [[UINavigationController alloc] initWithRootViewController:home];
                APP_DELEGATE.window.rootViewController = APP_DELEGATE.navController;
            });
        }
    } failure:^(NSError *error) {
        NSLog(@"Error");//error
    }];
}

#pragma mark - Image picker methods
-(void)didTapOnProfilePic: (UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:@"Choose"
                                                            preferredStyle:(UIAlertControllerStyleActionSheet)];
    
    UIAlertAction *alert_camera_action = [UIAlertAction actionWithTitle:@"Camera"
                                                                  style:(UIAlertActionStyleDefault)
                                                                handler:^(UIAlertAction * _Nonnull action) {
                                                                    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
                                                                        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                                                              message:@"Device does not support camera"
                                                                                                                             delegate:nil
                                                                                                                    cancelButtonTitle:@"OK"
                                                                                                                    otherButtonTitles: nil];
                                                                        [myAlertView show];
                                                                    }
                                                                    else {
                                                                        // open Camera
                                                                        pickerVC = [[UIImagePickerController alloc] init];
                                                                        pickerVC.delegate = self;
                                                                        pickerVC.allowsEditing = YES;
                                                                        pickerVC.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                                        [self presentViewController:pickerVC animated:YES completion:nil];
                                                                    }
                                                                }];
    UIAlertAction *alert_gallary_action = [UIAlertAction actionWithTitle:@"Gallary"
                                                                   style:(UIAlertActionStyleDefault)
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     // open Gallary
                                                                     pickerVC = [[UIImagePickerController alloc] init];
                                                                     pickerVC.delegate = self;
                                                                     pickerVC.allowsEditing = YES;
                                                                     pickerVC.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                                                     [self presentViewController:pickerVC animated:YES completion:nil];
                                                                 }];
    UIAlertAction *alert_cancel_action = [UIAlertAction actionWithTitle:@"Cancel"
                                                                  style:(UIAlertActionStyleCancel)
                                                                handler:^(UIAlertAction * _Nonnull action) {
                                                                    
                                                                }];
    [alert addAction:alert_camera_action];
    [alert addAction:alert_gallary_action];
    [alert addAction:alert_cancel_action];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
        self.imgView.contentMode = UIViewContentModeScaleAspectFit;
        self.imgView.image = [self shrinkImage:image toSize:self.imgView.bounds.size];
    }
    else {
        UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
        self.imgView.contentMode = UIViewContentModeScaleToFill;
        self.imgView.image = image;
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(NSString*)base64String:(UIImage*)image
{
    NSString *str=@"";
    str = [str  stringByAppendingString:[UIImageJPEGRepresentation(image, 0.8) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength]];
    return str;
}

#pragma mark - Custom methods
- (void)pickPlaceWithLocation: (CLLocation *)selectedLocation
{
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(selectedLocation.coordinate.latitude, selectedLocation.coordinate.longitude);
    CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(center.latitude + 0.001, center.longitude + 0.001);
    CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(center.latitude - 0.001, center.longitude - 0.001);
    
    GMSCoordinateBounds *viewPort = [[GMSCoordinateBounds alloc] initWithCoordinate:northEast coordinate:southWest];
    GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:viewPort];
    placePicker = [[GMSPlacePickerViewController alloc] initWithConfig:config];
    placePicker.delegate = self;
    [self presentViewController:placePicker animated:true completion:nil];
}

- (UIImage *)shrinkImage:(UIImage *)original toSize:(CGSize)size
{
    UIGraphicsBeginImageContextWithOptions(size, YES, 0);
    CGFloat originalAspect = original.size.width / original.size.height;
    CGFloat targetAspect = size.width / size.height;
    CGRect targetRect;
    
    if (originalAspect > targetAspect) {
        // original is wider than target
        targetRect.size.width = size.width;
        targetRect.size.height = size.height * targetAspect / originalAspect;
        targetRect.origin.x = 0;
        targetRect.origin.y = (size.height - targetRect.size.height) * 0.5;
    } else if (originalAspect < targetAspect) {
        // original is narrower than target
        targetRect.size.width = size.width * originalAspect / targetAspect;
        targetRect.size.height = size.height;
        targetRect.origin.x = (size.width - targetRect.size.width) * 0.5;
        targetRect.origin.y = 0;
    } else {
        // original and target have same aspect ratio
        targetRect = CGRectMake(0, 0, size.width, size.height);
    }
    [original drawInRect:targetRect];
    UIImage *final = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return final;
}

-(void)didSelectToolBarDone
{
    [editingTextField resignFirstResponder];
}
#pragma mark - GMSPlacePickerViewController methods
- (void)placePicker:(GMSPlacePickerViewController *)viewController didPickPlace:(GMSPlace *)place
{
    NSLog(@"%@",place);
    locLatitude = [NSString stringWithFormat:@"%f",place.coordinate.latitude];
    locLongitude = [NSString stringWithFormat:@"%f",place.coordinate.longitude];
    self.TF_Address.text = [NSString stringWithFormat:@"%@",place.formattedAddress];
    self.TF_GeoLocation.text = [NSString stringWithFormat:@"Geo Location : %f, %f",place.coordinate.latitude,place.coordinate.longitude];
    [viewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)placePicker:(GMSPlacePickerViewController *)viewController
   didFailWithError:(NSError *)error
{
    //[viewController dismissViewControllerAnimated:YES completion:nil];
}
- (void)placePickerDidCancel:(GMSPlacePickerViewController *)viewController
{
    [viewController dismissViewControllerAnimated:YES completion:nil];
}

@end
