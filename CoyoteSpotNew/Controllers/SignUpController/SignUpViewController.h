//
//  SignUpViewController.h
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Utilities.h"
#import "TPKeyboardAvoidingScrollView.h"

@interface SignUpViewController : UIViewController

@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *TF_Firstname;
@property (weak, nonatomic) IBOutlet UITextField *TF_Lastname;
@property (weak, nonatomic) IBOutlet UITextField *TF_EmailId;
@property (weak, nonatomic) IBOutlet UITextField *TF_Password;
@property (weak, nonatomic) IBOutlet UITextField *TF_Mobile;
@property (weak, nonatomic) IBOutlet UITextField *TF_Address;
@property (weak, nonatomic) IBOutlet UITextField *TF_GeoLocation;
@property (weak, nonatomic) IBOutlet UITextField *TF_ReEnterPass;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

- (IBAction)btnSubmitClicked:(id)sender;
- (IBAction)btnBackClicked:(id)sender;

@end
