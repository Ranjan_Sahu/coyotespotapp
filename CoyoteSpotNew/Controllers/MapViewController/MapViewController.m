//
//  MapViewController.m
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import "MapViewController.h"
#import "FormViewController.h"

@interface MapViewController ()<GMSMapViewDelegate,CLLocationManagerDelegate, MKMapViewDelegate>
{
    CLLocationManager *locationManager;
    CLLocation *userLocation, *choosenLocation;
    GMSGeocoder *geoCoder;
    GMSCameraPosition *camera;
    GMSMarker *marker;
    GMSCoordinateBounds *bounds;
    NSMutableArray *encounteredCoyoteList;
}
@end

@implementation MapViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lbl_locationInfo.hidden = true;
    locationManager = [[CLLocationManager alloc] init];
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager setDistanceFilter:100];
    [locationManager requestAlwaysAuthorization];
    [locationManager requestWhenInUseAuthorization];
    //[locationManager startMonitoringSignificantLocationChanges];
    locationManager.delegate = self;
    
    if( IOS_VERSION >=8.0)
    {
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
        {
            [locationManager requestWhenInUseAuthorization];
        }
    }
    if ([locationManager respondsToSelector:@selector(pausesLocationUpdatesAutomatically)])
    {
        locationManager.pausesLocationUpdatesAutomatically = NO;
    }
    
    [locationManager startUpdatingLocation];
    userLocation = [[CLLocation alloc]init];
    geoCoder = [GMSGeocoder new];
    marker = [[GMSMarker alloc] init];
    bounds = [[GMSCoordinateBounds alloc] init];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:TRUE];
    userLocation = locationManager.location;
    // Add notification observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotNotification) name:notification_Observer_Const object:nil];
    [self loadMapView];
    // get List
    [self callServiceToGetEncounteredCoyoteList];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification_Observer_Const object:nil];
}
#pragma mark - GMSMap methods
- (void)loadMapView
{
    //[super loadView];
    [self.view layoutIfNeeded];
    self.mapView.delegate = self;
    if(!([userLocation isKindOfClass:[NSNull class]] || userLocation == nil))
    {
        camera = [GMSCameraPosition cameraWithTarget:CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude) zoom:6];
        self.mapView.camera = camera;
        self.mapView.myLocationEnabled = YES;
        self.mapView.mapType = kGMSTypeNormal;
        marker.position = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
        marker.map = self.mapView;
    }
    else
    {
//        camera = [GMSCameraPosition cameraWithLatitude:-33.86 longitude:151.20 zoom:6];
//        self.mapView.camera = camera;
//        self.mapView.myLocationEnabled = YES;
//        self.mapView.mapType = kGMSTypeNormal;
//        
//        marker.position = CLLocationCoordinate2DMake(-33.86, 151.20);
//        marker.title = @"Sydney";
//        marker.snippet = @"Australia";
//        marker.map = self.mapView;
    }
    
}

-(void)addMarker
{
    if(encounteredCoyoteList)
    {
        for (int i=0;i<encounteredCoyoteList.count;i++)
        {
            NSDictionary *placeInfoDict =[encounteredCoyoteList objectAtIndex:i] ;
            if(placeInfoDict)
            {
                CGFloat latitude = [placeInfoDict[@"latitude"] floatValue];
                CGFloat longitude = [placeInfoDict[@"longitude"] floatValue];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    GMSMarker *marker1 = [[GMSMarker alloc] init];
                    marker1.position = CLLocationCoordinate2DMake(latitude, longitude);
                    marker1.title = placeInfoDict[@"coyote_type"];
                    marker1.snippet = placeInfoDict[@"message"];
                    marker1.icon = [UIImage imageNamed:@"Marker"];
                    bounds = [bounds includingCoordinate:marker1.position];
                    marker1.map = self.mapView;
                    [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
                });
                
            }
        }
    }
}

#pragma mark - CLLocationManagerDelegate Methods
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    userLocation = locations.lastObject;
    if (userLocation) {
        self.lbl_locationInfo.hidden = true;
    }
    NSString *userLatitude = [NSString stringWithFormat:@"%f",userLocation.coordinate.latitude];
    NSString *userLongitude = [NSString stringWithFormat:@"%f",userLocation.coordinate.longitude];
    [[NSUserDefaults standardUserDefaults] setValue:userLatitude forKey:@"USER_LATITUDE"];
    [[NSUserDefaults standardUserDefaults] setValue:userLongitude forKey:@"USER_LONGITUDE"];
    
    self.mapView.camera = [GMSCameraPosition cameraWithTarget:CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude) zoom:6];
    [self.mapView animateToLocation:CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude)];
    [self.mapView setMinZoom:1 maxZoom:20];
    
    marker.position = CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude);
    marker.map = self.mapView;
    
    [geoCoder reverseGeocodeCoordinate:CLLocationCoordinate2DMake(userLocation.coordinate.latitude, userLocation.coordinate.longitude) completionHandler:^(GMSReverseGeocodeResponse *response, NSError *error ) {
        
        GMSAddress *locationAddress = response.firstResult;
        marker.title = locationAddress.locality;
        marker.snippet = locationAddress.country;
        NSString *locationLineAddress = [NSString stringWithFormat:@"%@",locationAddress.lines];
        NSString *address = [NSString stringWithFormat:@"%@, %@",locationAddress.lines[0],locationAddress.locality];
        [[NSUserDefaults standardUserDefaults] setValue:address forKey:@"USER_ADDRESS"];
        // update location
        [self callServiceToUpdateDeviceLocationWithLat:userLatitude andLong:userLongitude andLocAdd:locationLineAddress];
        
    }];
    //[locationManager stopUpdatingLocation];
    
    // get List
    [self callServiceToGetEncounteredCoyoteList];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    self.lbl_locationInfo.hidden = false;
//    NSString *errorType = (error.code == kCLErrorDenied) ? @"Access Denied" : @"Unknown Error";
//    UIAlertView *alert = [[UIAlertView alloc]
//                          initWithTitle:@"Error getting Location"
//                          message:errorType
//                          delegate:nil
//                          cancelButtonTitle:@"OK"
//                          otherButtonTitles:nil];
//    [alert show];
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    if (status == kCLAuthorizationStatusDenied)
    {
        NSLog(@"kCLAuthorizationStatusDenied");
    }
    else if (status == kCLAuthorizationStatusAuthorizedAlways)
    {
        [locationManager startUpdatingLocation];
    }
    else if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        NSLog(@"kCLAuthorizationStatusAuthorizedWhenInUse");
    }
}

#pragma mark - Custom Methods
-(void)callServiceToUpdateDeviceLocationWithLat:(NSString *)latitude andLong: (NSString *)longitude andLocAdd: (NSString *)lineAddress
{
    WebserviceHelper *web = [[WebserviceHelper alloc] init];
    NSMutableDictionary *paramdict = [[NSMutableDictionary alloc]init];
    NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"DEVICE_TOKEN"];
    NSString *fcmDeviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"FCM_DEVICE_TOKEN"];
    
    if(token != nil)
    {
        paramdict[@"device_token"] = token;
        paramdict[@"fcm_device_token"] = fcmDeviceToken;
        paramdict[@"device_location"] = lineAddress;
        paramdict[@"latitude"] = latitude;
        paramdict[@"longitude"] = longitude;
        
        [web postJsonResponse:Service_UpdateCurrentLocation params:paramdict viewcontroller:self success:^(NSDictionary *response) {
            //success
            BOOL status = [response[@"status"] boolValue];
            if (!status){
                NSLog(@"Location Update failed");// Location Update failed
            } else {
                NSLog(@"Location Update success");// success Location Update done
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.lbl_locationInfo.hidden = true;
                });
            }
        } failure:^(NSError *error) {
            NSLog(@"Error");//error
        }];
    }
    else {
        NSLog(@"Can not update location : Device Token not found !");
    }
}

-(void)callServiceToGetEncounteredCoyoteList
{
    WebserviceHelper *web = [[WebserviceHelper alloc] init];
    NSMutableDictionary *paramdict = [[NSMutableDictionary alloc]init];
    
    NSString *userLat = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_LATITUDE"];
    NSString *userLong = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_LONGITUDE"];
    
    if (userLat != nil && userLong != nil) {
        paramdict[@"latitude"] = userLat;
        paramdict[@"longitude"] = userLong;
        
        [web postJsonResponse:Service_EncounterList params:paramdict viewcontroller:self success:^(NSDictionary *response) {
            //success
            BOOL status = [response[@"status"] boolValue];
            encounteredCoyoteList = [[NSMutableArray alloc]init];
            encounteredCoyoteList = response[@"data"];
            if (!status){
                NSLog(@"Get EncounteredCoyoteList failed");// Get EncounteredCoyoteList  failed
            } else {
                NSLog(@"Get EncounteredCoyoteList success");// success EncounteredCoyote save done
                if (encounteredCoyoteList.count>0) {
                    [self addMarker];
                }
            }
        } failure:^(NSError *error) {
            NSLog(@"Error");//error
        }];
    }
    else {
        NSLog(@"Can not Get EncounteredCoyoteList : location not found !");
    }
}

-(void)gotNotification
{
    // get List
    [self callServiceToGetEncounteredCoyoteList];
}
#pragma mark - IBAction Methods
- (IBAction)btnAlertClicked:(id)sender
{
    FormViewController *formcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"FormViewController"];
    [self.navigationController pushViewController:formcontroller animated:true];
}
- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

@end
