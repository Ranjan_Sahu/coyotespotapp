//
//  DetailFormViewController.m
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import "DetailFormViewController.h"

@interface DetailFormViewController ()<UITextFieldDelegate>
{
    NSString *coyoteType;
    UIToolbar *toolBar;
    UITextField *editingTextField;
}
@end

@implementation DetailFormViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    switch(_selectedIndex){
        case 1 :
            [self.btnTitle setTitle:@"Coyote Sighting" forState:UIControlStateNormal];
            self.lblCoyoteDesc.text = @"You have chosen a normal coyote sighting. This means that you are unsure if the coyote is agressive or non-agressive. This will show as a normal coyote sighting on the map.";
            coyoteType = @"Coyote Sighting";
            break; /* optional */
        case 2 :
            [self.btnTitle setTitle:@"Coyote Encounter" forState:UIControlStateNormal];
            self.lblCoyoteDesc.text = @"You have chosen coyote encounter. This means you have seen a coyote near you or your property. This will show up as an important sighting on the map.";
            coyoteType = @"Coyote Encounter";
            break; /* optional */
        case 3 :
            [self.btnTitle setTitle:@"Coyote: Unattended Attack" forState:UIControlStateNormal];
            self.lblCoyoteDesc.text = @"You have chosen an unattended coyote attack. This means that a coyote has attacked a pet which was alone. This will show up as an important event and will be reported to the police.";
            coyoteType = @"Coyote: Unattended Attack";
            break; /* optional */
        case 4 :
            [self.btnTitle setTitle:@"Coyote: Attended Attack" forState:UIControlStateNormal];
            self.lblCoyoteDesc.text = @"You have chosen an attended coyote attack. This means that a coyote has attacked a pet when a human was within a few feet of the pet; or has attacked a human. This will show up as a very important/critical sighting, and will be reported to the police.";
            coyoteType = @"Coyote: Attended Attack";
            break; /* optional */
    }
    
    //set delegate
    self.TF_Comment.delegate = self;
    // Toobar for Done button
    toolBar = [[UIToolbar alloc] init];
    [toolBar sizeToFit];
    UIBarButtonItem *flexBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                      target:nil action:nil];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc]
                                      initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                      target:self action:@selector(didSelectToolBarDone)];
    toolBar.items = @[flexBarButton, doneBarButton];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:TRUE];
    // Add notification observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotNotification) name:notification_Observer_Const object:nil];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification_Observer_Const object:nil];
}
-(void)gotNotification
{
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[MapViewController class]]) {
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
}

#pragma mark - TextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField{
    editingTextField = textField;
    textField.inputAccessoryView = toolBar;
    [textField becomeFirstResponder];
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    editingTextField = nil;
    [textField resignFirstResponder];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return true;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return true;
}

#pragma mark - Custom methods
-(void)didSelectToolBarDone
{
    [editingTextField resignFirstResponder];
}
#pragma mark - IBAction methods
- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)btnSubmitClicked:(id)sender
{
    [self.view endEditing:YES];
    [self callServiceToSaveEncounteredCoyote];
}

-(void)callServiceToSaveEncounteredCoyote
{
    WebserviceHelper *web = [[WebserviceHelper alloc] init];
    NSMutableDictionary *paramdict = [[NSMutableDictionary alloc]init];
    
    NSString *userId = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_ID"];
    NSString *userLat = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_LATITUDE"];
    NSString *userLong = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_LONGITUDE"];
    NSString *address = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_ADDRESS"];
    
    if (userLat != nil && userLong != nil && userId != nil) {
        paramdict[@"user_id"] = userId;
        paramdict[@"latitude"] = userLat;
        paramdict[@"longitude"] = userLong;
        paramdict[@"coyote_type"] = coyoteType;
        paramdict[@"message"] = self.TF_Comment.text;
        paramdict[@"address"] = address;
        
        if (self.TF_Comment.text.length == 0) {
            [Utilities showAlertWithTitle:@"" Message:@"Message is required !" CancelButtonTitle:@"Ok" InView:self];
        }
        else if ([address isKindOfClass:[NSNull class]] || address.length == 0) {
            [Utilities showAlertWithTitle:@"" Message:@"Address not found !" CancelButtonTitle:@"Ok" InView:self];
        }
        else if (coyoteType.length == 0) {
            [Utilities showAlertWithTitle:@"" Message:@"CoyoteType is required !" CancelButtonTitle:@"Ok" InView:self];
        }
        else {
            
            [web postJsonResponse:Service_SaveEncounter params:paramdict viewcontroller:self success:^(NSDictionary *response) {
                //success
                BOOL status = [response[@"status"] boolValue];
                if (!status){
                    NSLog(@"EncounteredCoyote save failed");// EncounteredCoyote save failed
                    [Utilities showAlertWithTitle:@"" Message:response[@"message"] CancelButtonTitle:@"Ok" InView:self];
                } else {
                    NSLog(@"EncounteredCoyote save success");// Success EncounteredCoyote save done
                    NSString *msg = [NSString stringWithFormat:@"%@",response[@"message"]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@""
                                                                                       message:msg
                                                                                preferredStyle:(UIAlertControllerStyleAlert)];
                        UIAlertAction *alert_cancel_action = [UIAlertAction actionWithTitle:@"Ok"
                                                                                      style:(UIAlertActionStyleCancel)
                                                                                    handler:^(UIAlertAction * _Nonnull action) {
                                                                                        [self.navigationController popViewControllerAnimated:true];
                                                                                    }];
                        [alert addAction:alert_cancel_action];
                        [self presentViewController:alert animated:YES completion:nil];
                    });
                    
                }
            } failure:^(NSError *error) {
                NSLog(@"Error");//error
            }];
        }
    }
    else {
        NSLog(@"Can not save Coyote : location or user id not found !");
        [Utilities showAlertWithTitle:@"" Message:@"Can not save Coyote : location or user id not found !" CancelButtonTitle:@"Ok" InView:self];
    }
    
}

@end
