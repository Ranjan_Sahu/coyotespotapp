//
//  DetailFormViewController.h
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailFormViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblCoyoteDesc;
@property (weak, nonatomic) IBOutlet UITextField *TF_Comment;
- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnSubmitClicked:(id)sender;

@property(nonatomic,assign)int selectedIndex;
@property (weak, nonatomic) IBOutlet TPKeyboardAvoidingScrollView *scrollView;

@end
