//
//  HomeViewController.m
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import "HomeViewController.h"
#import "MapViewController.h"
#import "SignUpViewController.h"
#import "SignInViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self callServiceToUpdateDeviceToken];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
    // Add notification observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotNotification) name:notification_Observer_Const object:nil];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification_Observer_Const object:nil];
}
- (IBAction)btnContMapClicked:(id)sender
{
    MapViewController *map = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    [self.navigationController pushViewController:map animated:true];
}

- (IBAction)didSelectLogout:(id)sender {
    [APP_DELEGATE logoutUser];
}
-(void)fetchData
{
    WebserviceHelper *web = [[WebserviceHelper alloc] init];
    [web getJsonResponse:@"" viewcontroller:self success:^(NSDictionary *response) {
        //success
    } failure:^(NSError *error) {
        //error
    }];
}

-(void)callServiceToUpdateDeviceToken
{
    WebserviceHelper *web = [[WebserviceHelper alloc] init];
    NSMutableDictionary *paramdict = [[NSMutableDictionary alloc]init];
    
    NSString *userId = [[NSUserDefaults standardUserDefaults] valueForKey:@"USER_ID"];
    NSString *token = [[NSUserDefaults standardUserDefaults] valueForKey:@"DEVICE_TOKEN"];
    NSString *fcmDeviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:@"FCM_DEVICE_TOKEN"];
    
    paramdict[@"user_id"] = userId;
    paramdict[@"device_token"] = token;
    paramdict[@"fcm_device_token"] = fcmDeviceToken;
    
    if (userId == nil || token == nil ||  fcmDeviceToken == nil)
    {
        NSLog(@"Can not update token : userid/token not found !");
    }
    else
    {
        [web postJsonResponse:Service_UpdateDeviceToken params:paramdict viewcontroller:self success:^(NSDictionary *response) {
            //success
            BOOL status = [response[@"status"] boolValue];
            if (!status){
                NSLog(@"Token Update failed");// Token Update failed
            } else {
                NSLog(@"Token Update success");// success Token Update done
            }
        } failure:^(NSError *error) {
            NSLog(@"Error");//error
        }];
        
    }
}
-(void)gotNotification
{
    // get List
    MapViewController *map = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    [self.navigationController pushViewController:map animated:true];
}


@end
