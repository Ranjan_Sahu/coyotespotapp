//
//  HomeViewController.h
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
- (IBAction)btnContMapClicked:(id)sender;
- (IBAction)didSelectLogout:(id)sender;

@end
