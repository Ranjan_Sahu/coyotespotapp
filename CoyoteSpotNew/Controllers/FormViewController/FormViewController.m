//
//  FormViewController.m
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import "FormViewController.h"
#import "DetailFormViewController.h"

@interface FormViewController ()
@end

@implementation FormViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setHidden:TRUE];
    // Add notification observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(gotNotification) name:notification_Observer_Const object:nil];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:notification_Observer_Const object:nil];
}
-(void)gotNotification
{
    for (UIViewController *vc in self.navigationController.viewControllers) {
        if ([vc isKindOfClass:[MapViewController class]]) {
            [self.navigationController popToViewController:vc animated:YES];
        }
    }
}


- (IBAction)btnBackClicked:(id)sender
{
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)btnCySightingClicked:(id)sender
{
    [self pushController:1];
}

- (IBAction)btnCyEncounterClicked:(id)sender
{
    [self pushController:2];
}

- (IBAction)btnCyUnattendedClicked:(id)sender
{
    [self pushController:3];
}

- (IBAction)btnCyAttendedClicked:(id)sender
{
    [self pushController:4];
}

-(void)pushController:(int)index
{
    DetailFormViewController *detailformcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailFormViewController"];
    detailformcontroller.selectedIndex = index;
    [self.navigationController pushViewController:detailformcontroller animated:true];
}

@end
