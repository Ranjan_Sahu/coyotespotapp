//
//  FormViewController.h
//  CoyoteSpot
//
//  Created by Admin on 24/10/17.
//  Copyright © 2017 RoboChaps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FormViewController : UIViewController

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnCySightingClicked:(id)sender;
- (IBAction)btnCyEncounterClicked:(id)sender;
- (IBAction)btnCyUnattendedClicked:(id)sender;
- (IBAction)btnCyAttendedClicked:(id)sender;

@end
